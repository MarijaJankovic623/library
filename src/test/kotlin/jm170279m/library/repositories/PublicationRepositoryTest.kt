package jm170279m.library.repositories

import jm170279m.library.entities.Publication
import jm170279m.library.entities.Publisher
import org.hamcrest.CoreMatchers.hasItem
import org.hamcrest.CoreMatchers.not
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThat
import org.junit.Test
import org.mockito.Mockito


class PublicationRepositoryTest {

    private lateinit var publciationRepository: PublicationRepository

    @Test
    fun noPublication_addPublication_publicationExists() {
        val publication = Mockito.mock(Publication::class.java)
        publciationRepository = PublicationRepository()

        publciationRepository.addPublication(publication)

        assertThat(publciationRepository.getPublications(), hasItem(publication))
    }

    @Test
    fun indexGiven_removePublication_noPublication() {
        val publication = Publication()
        publciationRepository = PublicationRepository()
        publciationRepository.addPublication(publication)

        publciationRepository.removePublication(0)

        assertThat(publciationRepository.getPublications(), not(hasItem(publication)))
    }

    @Test
    fun indexGiven_getPublication_correctPublication() {
        val publication = Publication()
        publciationRepository = PublicationRepository()
        publciationRepository.addPublication(publication)

        assertEquals(publication, publciationRepository.getPublication(0))
    }

    @Test
    fun publicationGiven_getPublication_correctPublication() {
        val publication = Publication()
        publciationRepository = PublicationRepository()
        publciationRepository.addPublication(publication)

        assertEquals(publication, publciationRepository.getPublication(publication.ID))
    }

    @Test
    fun noPublications_addSomePublications_countPublications() {
        val publication = Publication()
        publciationRepository = PublicationRepository()
        publciationRepository.addPublication(publication)

        assertEquals(1, publciationRepository.countPublications())
    }

    @Test
    fun givenPublisher_findPublicationsByPublisher_correctPublications() {
        val publisher = Mockito.mock(Publisher::class.java)
        val publication = Mockito.mock(Publication::class.java)
        publication.publisher = publisher
        publciationRepository = PublicationRepository()
        publciationRepository.addPublication(publication)

        assertThat(publciationRepository.getPublicationByPublisher(publisher), hasItem(publication))
    }


}
