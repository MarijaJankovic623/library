package jm170279m.library.services

import jm170279m.library.entities.Book
import jm170279m.library.entities.Publication
import jm170279m.library.entities.Publisher
import jm170279m.library.entities.User
import jm170279m.library.repositories.BookRepository
import jm170279m.library.repositories.PublicationRepository
import jm170279m.library.repositories.PublisherRepository
import jm170279m.library.repositories.UserRepository
import java.time.LocalDate
import java.util.*


object InitSingleton {


    fun initAll(userRepository: UserRepository, publisherRepository: PublisherRepository, publicationRepository: PublicationRepository, bookRepository: BookRepository) {
        var users: MutableList<User> = ArrayList()
        users.add(User("Ana", "Anic", UUID.fromString("11eee5d8-5ae3-4439-84e2-c99041f99e31")))
        users.add(User("Pera", "Peric", UUID.fromString("12eee5d8-5ae3-4439-84e2-c99041f99e31")))
        users.add(User("Mika", "Mikic", UUID.fromString("13eee5d8-5ae3-4439-84e2-c99041f99e31")))
        userRepository.initUsers(users)

        var publishers: MutableList<Publisher> = ArrayList()
        publishers.add(Publisher("Branko", "Brankic", UUID.fromString("21eee5d8-5ae3-4439-84e2-c99041f99e31")))
        publishers.add(Publisher("Zika", "Zikic", UUID.fromString("22eee5d8-5ae3-4439-84e2-c99041f99e31")))
        publishers.add(Publisher("Una", "Unic", UUID.fromString("23eee5d8-5ae3-4439-84e2-c99041f99e31")))
        publisherRepository.initPublishers(publishers)


        var publications: MutableList<Publication> = ArrayList()
        publications.add(Publication("Mesa Selimovic", "Dervis i smrt",
                LocalDate.now(), 2, 5000, "srp",
                UUID.fromString("31eee5d8-5ae3-4439-84e2-c99041f99e31")))
        publications.add(Publication("Mesa Selimovic", "Dervis i smrt",
                LocalDate.now(), 2, 5000, "srp",
                UUID.fromString("32eee5d8-5ae3-4439-84e2-c99041f99e31")))
        publications.add(Publication("Mesa Selimovic", "Dervis i smrt",
                LocalDate.now(), 2, 5000, "srp"))
        publications.add(Publication("Mesa Selimovic", "Dervis i smrt",
                LocalDate.now(), 2, 5000, "srp",
                UUID.fromString("33eee5d8-5ae3-4439-84e2-c99041f99e31")))
        publicationRepository.initPublications(publications)

        var books: MutableList<Book> = ArrayList()
        books.add(Book(publications[0], UUID.fromString("41eee5d8-5ae3-4439-84e2-c99041f99e31")))
        books.add(Book(publications[1], UUID.fromString("42eee5d8-5ae3-4439-84e2-c99041f99e31")))
        books.add(Book(publications[2], UUID.fromString("43eee5d8-5ae3-4439-84e2-c99041f99e31")))
        bookRepository.initBooks(books)

    }

}