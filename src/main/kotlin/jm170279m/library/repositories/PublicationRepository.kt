package jm170279m.library.repositories

import jm170279m.library.entities.Publication
import jm170279m.library.entities.Publisher
import org.springframework.stereotype.Repository
import java.util.*
import kotlin.collections.ArrayList

@Repository
class PublicationRepository {
    private var publications: MutableList<Publication> = ArrayList()

    private var wishListener: WishListener? = null

    fun setWishlistener(wishListener: WishListener) {
        this.wishListener = wishListener
    }


    fun initPublications(publications: MutableList<Publication>) {
        this.publications = publications
    }

    fun getPublications(): List<Publication> = Collections.unmodifiableList(publications)

    fun addPublication(publication: Publication) {
        publications.add(publication)
        wishListener?.onNewPublication(publication)
    }

    fun removePublication(index: Int) = publications.removeAt(index)

    fun removePublication(publication: Publication) = publications.remove(publication)

    fun getPublication(ID: UUID) = publications.find { p -> p.ID == ID }

    fun getPublication(index: Int) = publications[index]

    fun countPublications() = publications.size

    fun getPublicationByPublisher(publisher: Publisher) = publications.filter { publication -> publication.publisher == publisher }
}