package jm170279m.library.entitiesDTO

import jm170279m.library.entities.UserDTO

open class PublisherDTO(username: String) : UserDTO(username) {

    override fun toString(): String {
        return "PUBLISHER:" + username
    }
}