package jm170279m.library.controllers

import jm170279m.library.entities.Publication
import jm170279m.library.repositories.PublisherService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping(value = "/publisher")
class PublisherController @Autowired constructor(private var publisherService: PublisherService) {

    val logger = LoggerFactory.getLogger(PublisherController::class.java)

    @RequestMapping(value = "/publications/{publisherID}", method = arrayOf(RequestMethod.GET))
    fun seePublications(@PathVariable publisherID: UUID): List<Publication> {
        var publisher = publisherService.publisherRepository.getPublisher(publisherID)
        return publisherService.seePublications(publisher!!)
    }

    @RequestMapping(value = "/createPublication/{publisherID}", method = arrayOf(RequestMethod.POST))
    fun createPublication(@RequestBody publication: Publication, @PathVariable publisherID: UUID): ResponseEntity<String> {
        var publisher = publisherService.publisherRepository.getPublisher(publisherID)
        publisherService.createPublicationForPublisher(publication, publisher!!)
        return ResponseEntity<String>("Publication successfully created", HttpStatus.CREATED)
    }

    @RequestMapping(value = "/deletePublication/{publisherID}", method = arrayOf(RequestMethod.DELETE))
    fun deletePublication(@PathVariable publicationID: UUID, @PathVariable publisherID: UUID): ResponseEntity<String> {
        var publisher = publisherService.publisherRepository.getPublisher(publisherID)
        var publication = publisherService.publicationRepository.getPublication(publicationID)
        if (publisher == publication!!.publisher) {
            publisherService.deletePublication(publication!!)
            return ResponseEntity<String>("Publication successfully deleted", HttpStatus.OK)
        }
        return ResponseEntity<String>("Not authorized to delete this publication", HttpStatus.METHOD_NOT_ALLOWED)
    }

}