package jm170279m.library.controllers

import jm170279m.library.entities.Book
import jm170279m.library.entities.Publication
import jm170279m.library.entities.User
import jm170279m.library.repositories.UserService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import java.util.*


@RestController
@RequestMapping(value = "/users")
class UserController @Autowired constructor(private var userService: UserService) {

    val logger = LoggerFactory.getLogger(UserController::class.java)

    @RequestMapping(value = "/{userId}", method = arrayOf(RequestMethod.GET))
    fun getUser(@PathVariable userId: UUID): User? {
        return userService.userRepository.getUser(userId)
    }

    @RequestMapping(value = "/getUser/{username}", method = arrayOf(RequestMethod.GET))
    fun getUser(@PathVariable username: String): User? {
        return userService.userRepository.getUser(username)
    }

    @RequestMapping(value = "/publications", method = arrayOf(RequestMethod.GET))
    fun getPublications(): List<Publication> {
        return userService.publicationRepository.getPublications()
    }


    @RequestMapping(value = "/buy/{publicationID}/{userID}", method = arrayOf(RequestMethod.POST))
    fun buyBook(@PathVariable publicationID: UUID, @PathVariable userID: UUID): ResponseEntity<String> {
        var user = userService.userRepository.getUser(userID)
        var publication = userService.publicationRepository.getPublication(publicationID)
        userService.buyBook(publication!!, user!!)
        return ResponseEntity<String>("Book successfully bought", HttpStatus.CREATED)
    }

    @RequestMapping(value = "/sell/{bookID}/{userID}", method = arrayOf(RequestMethod.PUT))
    fun sellBook(@PathVariable bookID: UUID, @PathVariable userID: UUID): ResponseEntity<String> {
        var user = userService.userRepository.getUser(userID)
        var book = userService.bookRepository.getBook(bookID)
        userService.sellBook(book!!, user!!)
        return ResponseEntity<String>("Book successfully sold", HttpStatus.OK)
    }

    @RequestMapping(value = "/borrow/{publicationID}/{userID}", method = arrayOf(RequestMethod.PUT))
    fun findAndBorrowBook(@PathVariable publicationID: UUID, @PathVariable userID: UUID): ResponseEntity<String> {
        var userBorrowing = userService.userRepository.getUser(userID)
        var publication = userService.publicationRepository.getPublication(publicationID)
        userService.findAndBorrowBook(publication!!, userBorrowing!!)
        return ResponseEntity<String>("Book successfully borrowed", HttpStatus.OK)
    }

    @RequestMapping(value = "/return/{bookID}/{userID}", method = arrayOf(RequestMethod.PUT))
    fun returnBook(@PathVariable bookID: UUID, @PathVariable userID: UUID): ResponseEntity<String> {
        var user = userService.userRepository.getUser(userID)
        var book = userService.bookRepository.getBook(bookID)
        userService.returnBook(book!!, user!!)
        return ResponseEntity<String>("Book successfully returned", HttpStatus.OK)
    }

    @RequestMapping(value = "/library/{userID}", method = arrayOf(RequestMethod.GET))
    fun seeLibrary(@PathVariable userID: UUID): List<Book> {
        var user = userService.userRepository.getUser(userID)
        return userService.seeLibrary(user!!)
    }
}