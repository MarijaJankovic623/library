package jm170279m.library.entities

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import java.time.LocalDate

class PublicationTest {

    private lateinit var publication: Publication


    @Test
    fun noPublication_createPublication_IDExists() {
        publication = createDefaultPublication()

        assertNotNull(publication.ID)
    }

    @Test
    fun setTitle_getTitle_titleValid() {
        publication = createDefaultPublication()
        val titleTest = "Title"

        publication.title = titleTest

        assertEquals(titleTest, publication.title)
    }

    @Test
    fun setAuthor_getAuthor_authorValid() {
        publication = createDefaultPublication()
        val authorTest = "Author"

        publication.author = authorTest

        assertEquals(authorTest, publication.author)
    }

    @Test
    fun setPublishingDate_getPublishingDate_publishingDateValid() {
        publication = createDefaultPublication()
        val testDate: LocalDate = LocalDate.now()

        publication.publishingDate = testDate

        assertEquals(testDate, publication.publishingDate)
    }

    @Test
    fun setCopiesNumber_getCopiesNumber_copiesNumberValid() {
        publication = createDefaultPublication()
        val copiesNumberTest = 10

        publication.copiesNumber = copiesNumberTest

        assertEquals(copiesNumberTest, publication.copiesNumber)
    }

    @Test
    fun setwordNumber_getwordNumber_wordNumberValid() {
        publication = createDefaultPublication()
        val wordNumberTest = 10

        publication.wordNumber = wordNumberTest

        assertEquals(wordNumberTest, publication.wordNumber)
    }

    @Test
    fun setlanguage_getlanguage_languageValid() {
        publication = createDefaultPublication()
        val languageTest = "eng"

        publication.language = languageTest

        assertEquals(languageTest, publication.language)
    }

    private fun createDefaultPublication() = Publication("TestAuthor", "Testtitle", LocalDate.of(2016, 3, 3), 0, 0, "eng")

}