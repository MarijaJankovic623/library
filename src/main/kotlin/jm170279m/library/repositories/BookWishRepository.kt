package jm170279m.library.repositories

import jm170279m.library.entities.Book
import jm170279m.library.entities.BookWish
import jm170279m.library.entities.Publication
import jm170279m.library.entities.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import java.util.*


interface WishListener {
    fun onNewPublication(publication: Publication)
}


@Repository
class BookWishRepository @Autowired constructor(var bookRepository: BookRepository) : WishListener {
    private var wishes: MutableList<BookWish> = ArrayList()

    fun getWishes(): List<BookWish> = Collections.unmodifiableList(wishes)

    fun addWish(wish: BookWish) = wishes.add(wish)

    fun getWishsByUser(user: User) = wishes.filter { wishe -> wishe.user == user }

    override fun onNewPublication(publication: Publication) {
        var wish = wishes.find { wish -> wish.title === publication.title && wish.author === publication.author }
        var book = Book(publication!!)
        book.user = wish!!.user
        bookRepository.addBook(book)
        wishes.remove(wish)
    }

}