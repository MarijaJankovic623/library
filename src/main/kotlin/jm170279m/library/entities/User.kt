package jm170279m.library.entities

import java.util.*


open class User {
    var username: String
    private var password: String
    var ID: UUID

    constructor(username: String = "Generic", password: String = "Generis") {
        this.username = username
        this.password = password
        ID = UUID.randomUUID()
    }

    constructor(username: String, password: String, ID: UUID) {
        this.username = username
        this.password = password
        this.ID = ID
    }

}