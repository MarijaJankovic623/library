package jm170279m.library.controllers

import jm170279m.library.entities.Publication
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import java.time.LocalDate
import java.util.*

@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PublisherControllerTest {
    @Autowired
    lateinit var publisherController: PublisherController

    @Test
    fun seePublications() {
        var result = publisherController.seePublications(UUID.fromString("21eee5d8-5ae3-4439-84e2-c99041f99e31"))

        assertNotNull(result)
    }

    @Test
    fun createPublication() {
        var result: ResponseEntity<String> = publisherController.createPublication(
                Publication("Mesa Selimovic", "Dervis i smrt",
                        LocalDate.now(), 2, 5000, "srp",
                        UUID.fromString("31eee5d8-5ae3-4439-84e2-c99041f99e31")),
                UUID.fromString("21eee5d8-5ae3-4439-84e2-c99041f99e31"))

        assertEquals(HttpStatus.CREATED, result.statusCode)
    }

    @Test
    fun deletePublication() {
        publisherController.createPublication(
                Publication("Mesa Selimovic", "Dervis i smrt",
                        LocalDate.now(), 2, 5000, "srp",
                        UUID.fromString("35eee5d8-5ae3-4439-84e2-c99041f99e31")),
                UUID.fromString("21eee5d8-5ae3-4439-84e2-c99041f99e31"))


        var result: ResponseEntity<String> = publisherController.deletePublication(
                UUID.fromString("35eee5d8-5ae3-4439-84e2-c99041f99e31"),
                UUID.fromString("21eee5d8-5ae3-4439-84e2-c99041f99e31"))

        assertEquals(HttpStatus.OK, result.statusCode)
    }

}