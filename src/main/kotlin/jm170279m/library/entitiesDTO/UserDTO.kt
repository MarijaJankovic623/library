package jm170279m.library.entities


open class UserDTO(var username: String) {
    override fun toString(): String {
        return "USER:" + username
    }
}