package jm170279m.library.repositories

import jm170279m.library.entities.Book
import jm170279m.library.entities.Publication
import jm170279m.library.entities.User
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class BookRepository {
    private var books: MutableList<Book> = ArrayList()

    fun initBooks(books: MutableList<Book>) {
        this.books = books
    }

    fun getBooks(): List<Book> = Collections.unmodifiableList(books)

    fun addBook(book: Book) = books.add(book)


    fun removeBook(book: Book) = books.remove(book)

    fun removeBook(index: Int) = books.removeAt(index)

    fun getBookByPublication(publication: Publication) = books.find { book -> book.publication == publication }

    fun getBook(bookID: UUID) = books.find { b -> b.ID == bookID }

    fun getBook(index: Int) = books[index]

    fun countBooks() = books.size

    fun getBooksByUser(user: User) = books.filter { book -> book.user == user }

    fun getBorrowedBooksForUser(user: User) = books.filter { book -> book.userBorrowed == user }

}