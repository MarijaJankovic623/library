package jm170279m.library.repositories

import jm170279m.library.entities.Book
import jm170279m.library.entities.Publication
import jm170279m.library.entities.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserService @Autowired constructor(var userRepository: UserRepository, var bookRepository: BookRepository, var publicationRepository: PublicationRepository) {


    fun buyBook(publication: Publication, user: User) {
        var book = Book(publication!!)
        book.user = user
        bookRepository.addBook(book)
    }

    fun sellBook(book: Book, user: User) {
        book.user = null
        bookRepository.removeBook(book)

    }

    fun findAndBorrowBook(publication: Publication, userBorrowing: User): Boolean {
        var book: Book? = bookRepository.getBookByPublication(publication)
        if (book != null) {
            return book.borrowBook(userBorrowing)
        } else return false

    }

    fun returnBook(book: Book, user: User): Boolean {
        return book.returnBook(user)
    }

    fun seeLibrary(user: User): List<Book> {
        return bookRepository.getBooksByUser(user)
    }

    fun allBorrowedBooks(user: User): List<Book> {
        return bookRepository.getBorrowedBooksForUser(user)
    }
}