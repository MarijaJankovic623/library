package jm170279m.library

import jm170279m.library.controllers.IndexController
import jm170279m.library.controllers.PublisherController
import jm170279m.library.controllers.UserController
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner


@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AppTest {
    @Autowired
    lateinit var userController: UserController
    @Autowired
    lateinit var indexController: IndexController
    @Autowired
    lateinit var publisherController: PublisherController

    @Test
    fun test() {
        val result = indexController.index()
        assertEquals("Welcome to Library app\n" +
                "Possible routes\n" +
                "1. Create user\n" +
                "/user/\n" +
                "2. Log in as a user\n" +
                "/user/\n" +
                "3.Create publisher\n" +
                "/publisher/\n" +
                "4. Log in as a publisher\n" +
                "/publisher/", result)
    }


}