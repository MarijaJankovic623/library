package jm170279m.library.repositories


import jm170279m.library.entities.User
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mockito

class UserRepositoryTest {
    lateinit var userRepository: UserRepository

    @Test
    fun userGiven_getUser_correctUser() {
        userRepository = UserRepository()
        var user = Mockito.mock(User::class.java)
        user.username = "TestUsername"

        userRepository.addUser(user)

        assertNotNull(userRepository.getUsers())
    }

    @Test
    fun noUser_addUser_getUser() {
        userRepository = UserRepository()
        var user = User()

        userRepository.addUser(user)

        assertEquals(user, userRepository.getUser(user.ID))
    }


    @Test
    fun userExists_removeUser_noUser() {
        userRepository = UserRepository()
        var user = User()
        userRepository.addUser(user)

        userRepository.removeUser(user)

        assertTrue(userRepository.getUser(user.ID) == null)

    }

    @Test
    fun noUsers_addSomeUsers_countUsers() {
        userRepository = UserRepository()
        var user = Mockito.mock(User::class.java)

        userRepository.addUser(user)

        assertEquals(1, userRepository.countUsers())
    }

}