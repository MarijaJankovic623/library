package jm170279m.library.controllers

import jm170279m.library.entities.User
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import java.util.*

@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserControllerTest {
    @Autowired
    lateinit var userController: UserController


    @Test
    fun getUser() {
        val result: User? = userController.getUser("Ana")

        if (result != null) {
            assertEquals("Ana", result.username)
        }
    }

    @Test
    fun getUserID() {
        val result: User? = userController.getUser(UUID.fromString("11eee5d8-5ae3-4439-84e2-c99041f99e31"))

        if (result != null) {
            assertEquals("Ana", result.username)
        }
    }

    @Test
    fun getPublications() {
        var result = userController.getPublications()

        assertNotNull(result)
    }

    @Test
    fun buyBook() {
        var result: ResponseEntity<String> = userController.buyBook(UUID.fromString("31eee5d8-5ae3-4439-84e2-c99041f99e31"), UUID.fromString("11eee5d8-5ae3-4439-84e2-c99041f99e31"))

        assertEquals(HttpStatus.CREATED, result.statusCode)
    }

    @Test
    fun sellBook() {
        var result = userController.sellBook(UUID.fromString("41eee5d8-5ae3-4439-84e2-c99041f99e31"), UUID.fromString("11eee5d8-5ae3-4439-84e2-c99041f99e31"))

        assertEquals(HttpStatus.OK, result.statusCode)
    }

    @Test
    fun findAndBorrowBook() {
        var result = userController.findAndBorrowBook(UUID.fromString("32eee5d8-5ae3-4439-84e2-c99041f99e31"), UUID.fromString("11eee5d8-5ae3-4439-84e2-c99041f99e31"))

        assertEquals(HttpStatus.OK, result.statusCode)
    }


    @Test
    fun returnBook() {
        var result = userController.returnBook(UUID.fromString("42eee5d8-5ae3-4439-84e2-c99041f99e31"), UUID.fromString("11eee5d8-5ae3-4439-84e2-c99041f99e31"))

        assertEquals(HttpStatus.OK, result.statusCode)
    }


    @Test
    fun seeLibrary() {
        var result = userController.seeLibrary(UUID.fromString("11eee5d8-5ae3-4439-84e2-c99041f99e31"))

        assertNotNull(result)
    }

}