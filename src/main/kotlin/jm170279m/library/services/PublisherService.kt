package jm170279m.library.repositories

import jm170279m.library.entities.Publication
import jm170279m.library.entities.Publisher
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PublisherService @Autowired constructor(var publicationRepository: PublicationRepository, var publisherRepository: PublisherRepository) {

    fun seePublications(publisher: Publisher): List<Publication> {
        return publicationRepository.getPublicationByPublisher(publisher)
    }

    fun createPublicationForPublisher(publication: Publication, publisher: Publisher) {
        publication.publisher = publisher
        publicationRepository.addPublication(publication)
    }

    fun deletePublication(publication: Publication) {
        publicationRepository.removePublication(publication)
    }
}