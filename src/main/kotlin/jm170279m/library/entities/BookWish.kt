package jm170279m.library.entities

//prototype design pattern as data class in kotlin, compiler add copy method by default

data class BookWish constructor(var user: User, var author: String, var title: String)