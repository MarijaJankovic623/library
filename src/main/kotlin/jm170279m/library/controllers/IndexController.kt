package jm170279m.library.controllers

import jm170279m.library.repositories.BookRepository
import jm170279m.library.repositories.PublicationRepository
import jm170279m.library.repositories.PublisherRepository
import jm170279m.library.repositories.UserRepository
import jm170279m.library.services.InitSingleton
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class IndexController @Autowired constructor(var userRepository: UserRepository, var publisherRepository: PublisherRepository, var publicationRepository: PublicationRepository, var bookRepository: BookRepository) {

    var init = InitSingleton

    init {
        init.initAll(userRepository, publisherRepository, publicationRepository, bookRepository)
    }


    @RequestMapping(value = "/")
    fun index(): String {
        return "Welcome to Library app\n"
    }

}