package jm170279m.library.repositories

import jm170279m.library.entities.Publisher
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mockito.mock


class PublisherRepositoryTest {
    lateinit var publisherRepository: PublisherRepository

    @Test
    fun publisherGiven_getPublisher_correctPublisher() {
        publisherRepository = PublisherRepository()
        var publisher = mock(Publisher::class.java)
        publisher.username = "TestUsername"

        publisherRepository.addPublisher(publisher)

        assertNotNull(publisherRepository.getPublishers())
    }

    @Test
    fun noPublisher_addPublisher_getPublisher() {
        publisherRepository = PublisherRepository()
        var publisher = Publisher()

        publisherRepository.addPublisher(publisher)

        assertEquals(publisher, publisherRepository.getPublisher(publisher.ID))
    }


    @Test
    fun publisherExists_removePublisher_noPublisher() {
        publisherRepository = PublisherRepository()
        var publisher = Publisher()
        publisherRepository.addPublisher(publisher)

        publisherRepository.removePublisher(publisher)

        assertTrue(publisherRepository.getPublisher(publisher.ID) == null)

    }

    @Test
    fun noPublishers_addSomePublishers_countPublishers() {
        publisherRepository = PublisherRepository()
        var publisher = mock(Publisher::class.java)

        publisherRepository.addPublisher(publisher)

        assertEquals(1, publisherRepository.countPublishers())
    }


}