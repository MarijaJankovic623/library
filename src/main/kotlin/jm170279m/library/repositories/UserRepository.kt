package jm170279m.library.repositories

import jm170279m.library.entities.User
import jm170279m.library.entities.UserDTO
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class UserRepository {
    private var users: MutableList<User> = ArrayList()

    fun initUsers(users: MutableList<User>) {
        this.users = users
    }

    //DTO pattern
    fun getUsers(): List<UserDTO> {
        var usersDTO: MutableList<UserDTO> = ArrayList()
        for (user in users) {
            usersDTO.add(UserDTO(user.username))
        }
        return usersDTO
    }

    fun getUser(ID: UUID) = users.find { user -> user.ID == ID }

    fun addUser(user: User) = users.add(user)

    fun removeUser(user: User) = users.remove(user)

    fun countUsers() = users.size

    fun getUser(username: String) = users.find { user -> user.username == username }


}