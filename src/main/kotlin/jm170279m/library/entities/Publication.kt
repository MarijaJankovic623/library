package jm170279m.library.entities

import java.time.LocalDate
import java.util.*
import kotlin.properties.Delegates

open class Publication {
    var author: String
    var title: String
    var publishingDate: LocalDate
    var copiesNumber: Int by Delegates.observable(0) { prop, old, new ->
        println("Number of copies is changed")
    }
    var wordNumber: Int
    var language: String
    var ID: UUID
    var publisher: Publisher? = null

    constructor(author: String = "Generic", title: String = "Generic", publishingDate: LocalDate = LocalDate.of(2016, 3, 3), copiesNumber: Int = 0, wordNumber: Int = 0, language: String = "eng") {
        this.author = author
        this.title = title
        this.publishingDate = publishingDate
        this.copiesNumber = copiesNumber
        this.wordNumber = wordNumber
        this.language = language
        this.ID = UUID.randomUUID()
    }

    constructor(author: String, title: String, publishingDate: LocalDate, copiesNumber: Int, wordNumber: Int, language: String, ID: UUID) {
        this.author = author
        this.title = title
        this.publishingDate = publishingDate
        this.copiesNumber = copiesNumber
        this.wordNumber = wordNumber
        this.language = language
        this.ID = ID
    }
}