package jm170279m.library.repositories

import jm170279m.library.entities.Publisher
import jm170279m.library.entitiesDTO.PublisherDTO
import org.springframework.stereotype.Repository
import java.util.*
import kotlin.collections.ArrayList

@Repository
class PublisherRepository {
    private var publishers: MutableList<Publisher> = ArrayList()

    fun initPublishers(publishers: MutableList<Publisher>) {
        this.publishers = publishers
    }

    fun getPublisher(ID: UUID) = publishers.find { publisher -> publisher.ID.equals(ID) }

    //DTO pattern
    fun getPublishers(): List<PublisherDTO> {
        var publishersDTO: MutableList<PublisherDTO> = ArrayList()
        for (publisher in publishers) {
            publishersDTO.add(PublisherDTO(publisher.username))
        }
        return publishersDTO
    }

    fun addPublisher(publisher: Publisher) = publishers.add(publisher)

    fun removePublisher(publisher: Publisher) = publishers.remove(publisher)

    fun countPublishers() = publishers.size


}
