package jm170279m.library.repositories

import jm170279m.library.entities.Book
import jm170279m.library.entities.Publication
import jm170279m.library.entities.User
import org.hamcrest.CoreMatchers.hasItem
import org.hamcrest.CoreMatchers.not
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThat
import org.junit.Test
import org.mockito.Mockito

class BookRepositoryTest {
    private lateinit var bookRepository: BookRepository

    @Test
    fun noBook_addBook_bookExists() {
        val book = Mockito.mock(Book::class.java)
        bookRepository = BookRepository()

        bookRepository.addBook(book)

        assertThat(bookRepository.getBooks(), hasItem(book))
    }

    @Test
    fun indexGiven_removeBook_noBook() {
        val book = Mockito.mock(Book::class.java)
        bookRepository = BookRepository()
        bookRepository.addBook(book)

        bookRepository.removeBook(0)

        assertThat(bookRepository.getBooks(), not(hasItem(book)))
    }

    @Test
    fun bookGiven_removeBook_noBook() {
        val book = Mockito.mock(Book::class.java)
        bookRepository = BookRepository()
        bookRepository.addBook(book)

        bookRepository.removeBook(book)

        assertThat(bookRepository.getBooks(), not(hasItem(book)))
    }

    @Test
    fun indexGiven_getBook_correctBook() {
        val book = Mockito.mock(Book::class.java)
        bookRepository = BookRepository()
        bookRepository.addBook(book)

        assertEquals(book, bookRepository.getBook(0))
    }

    @Test
    fun bookGiven_getBook_correctBook() {
        val book = Book(Mockito.mock(Publication::class.java))
        bookRepository = BookRepository()
        bookRepository.addBook(book)

        assertEquals(book, bookRepository.getBook(book.ID))
    }

    @Test
    fun noBooks_addSomeBooks_countBooks() {
        val book = Mockito.mock(Book::class.java)
        bookRepository = BookRepository()
        bookRepository.addBook(book)

        assertEquals(1, bookRepository.countBooks())
    }

    @Test
    fun givenPublication_findBookByPublication_correctBook() {
        val publication = Mockito.mock(Publication::class.java)
        val book = Book(publication)
        bookRepository = BookRepository()
        bookRepository.addBook(book)

        assertEquals(bookRepository.getBookByPublication(publication), book)
    }

    @Test
    fun givenUser_findBooksByUser_correctBooks() {
        val user = Mockito.mock(User::class.java)
        val book = Mockito.mock(Book::class.java)
        book.user = user
        bookRepository = BookRepository()
        bookRepository.addBook(book)


        assertThat(bookRepository.getBooksByUser(user), hasItem(book))
    }




}