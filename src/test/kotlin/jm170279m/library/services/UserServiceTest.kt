package jm170279m.library.services

import jm170279m.library.entities.Book
import jm170279m.library.entities.Publication
import jm170279m.library.entities.User
import jm170279m.library.repositories.BookRepository
import jm170279m.library.repositories.PublicationRepository
import jm170279m.library.repositories.UserRepository
import jm170279m.library.repositories.UserService
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mockito

class UserServiceTest {
    var userService: UserService = UserService(UserRepository(), BookRepository(), PublicationRepository())

    //TODO find out how to make this test unit, because list in repositories is null when mocking them

    @Test
    fun givenID_getUser_rightUser() {
        var user = User()
        userService.userRepository.addUser(user)

        assertEquals(user, userService.userRepository.getUser(user.ID))
    }

    @Test
    fun givenUsername_getUser_rightUser() {
        var user = User()
        userService.userRepository.addUser(user)

        assertEquals(user, userService.userRepository.getUser(user.username))
    }

    @Test
    fun userGiven_buyBook_bookInLibrary() {
        val user = User()
        val publication = Publication()
        val booknumber = userService.seeLibrary(user).size

        userService.buyBook(publication, user)

        assertEquals(booknumber + 1, userService.seeLibrary(user).size)
    }

    @Test
    fun userAndBookGiven_sellBook_bookNotInLibrary() {
        val user = User()
        val publication = Publication()
        val booknumber = userService.seeLibrary(user).size
        userService.buyBook(publication, user)

        userService.sellBook(Mockito.mock(Book::class.java), user)

        assertEquals(booknumber + 1, userService.seeLibrary(user).size)
    }

    @Test
    fun publicationUserGiven_borrowBook_bookBorrowedOK() {
        var userBorrowing = User()
        var publication = Mockito.mock(Publication::class.java)
        var book = Mockito.mock(Book::class.java)
        book.publication = publication
        userService.bookRepository.addBook(book)

        val result = userService.findAndBorrowBook(publication, userBorrowing)

        assertTrue(result)
    }

    @Test
    fun publicationUserGiven_returnBook_bookReturnedOK() {
        var userBorrowing = User()
        var publication = Mockito.mock(Publication::class.java)
        var book = Mockito.mock(Book::class.java)
        book.publication = publication
        userService.bookRepository.addBook(book)
        userService.findAndBorrowBook(publication, userBorrowing)

        val result = userService.returnBook(book, userBorrowing)

        assertTrue(result)
    }

}