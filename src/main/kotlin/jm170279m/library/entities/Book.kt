package jm170279m.library.entities

import java.util.*


open class Book {
    var publication: Publication
    var ID: UUID
    var isBorrowed = false
    var user: User? = null
    var userBorrowed: User? = null

    constructor(publication: Publication) {
        this.publication = publication
        this.ID = UUID.randomUUID()
    }

    constructor(publication: Publication, ID: UUID) {
        this.publication = publication
        this.ID = ID
    }

    fun borrowBook(user: User): Boolean {
        if (!isBorrowed) {
            isBorrowed = true
            userBorrowed = user
            return true
        } else return false
    }

    fun returnBook(user: User): Boolean {
        if (isBorrowed && user == userBorrowed) {
            userBorrowed = null
            isBorrowed = false
            return true
        }
        return false
    }

}