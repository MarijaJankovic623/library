package jm170279m.library.entities

import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mockito

class BookTest {
    private lateinit var book: Book

    @Test
    fun bookBorrowed_returnBook_isBorrowedFalse() {
        book = Book(Mockito.mock(Publication::class.java))
        val user = Mockito.mock(User::class.java)
        book.borrowBook(user)

        book.returnBook(user)

        assertFalse(book.isBorrowed)
    }

    @Test
    fun bookBorrowed_returnBookByWrongUser_nothingChanges() {
        book = Book(Mockito.mock(Publication::class.java))
        val firstUser = Mockito.mock(User::class.java)
        book.borrowBook(firstUser)
        val secondUser = Mockito.mock(User::class.java)

        book.returnBook(secondUser)

        assertEquals(firstUser, book.userBorrowed)
    }

    @Test
    fun bookNotBorrowed_returnBook_isBorrowedFalseNoError() {
        book = Book(Mockito.mock(Publication::class.java))
        val user = Mockito.mock(User::class.java)

        book.returnBook(user)

        assertFalse(book.isBorrowed)
    }


    @Test
    fun bookNotBorrowed_borrowBook_isBorrowedTrue() {
        book = Book(Mockito.mock(Publication::class.java))
        val user = Mockito.mock(User::class.java)

        book.borrowBook(user)

        assertTrue(book.isBorrowed)
    }


    @Test
    fun bookBorrowed_borrowBook_userStaysTheSame() {
        book = Book(Mockito.mock(Publication::class.java))
        val firstUser = Mockito.mock(User::class.java)
        book.borrowBook(firstUser)
        val secondUser = Mockito.mock(User::class.java)

        book.borrowBook(secondUser)

        assertEquals(firstUser, book.userBorrowed)
    }


    @Test
    fun bookWithoutUser_setUser_userExists() {
        book = Book(Mockito.mock(Publication::class.java))

        book.user = Mockito.mock(User::class.java)

        assertNotNull(book.user)
    }

    @Test
    fun newBook_setPublication_publicationExists() {
        var publication = Mockito.mock(Publication::class.java)
        book = Book(publication)

        assertEquals(publication, book.publication)
    }

}