package jm170279m.library.services

import jm170279m.library.entities.Publication
import jm170279m.library.entities.Publisher
import jm170279m.library.repositories.PublicationRepository
import jm170279m.library.repositories.PublisherRepository
import jm170279m.library.repositories.PublisherService
import org.hamcrest.CoreMatchers.*
import org.junit.Test
import org.mockito.Mockito
import org.junit.Assert.*

class PublisherServiceTest {

    var publisherService: PublisherService = PublisherService(PublicationRepository(), PublisherRepository())

    @Test
    fun givenPublications_seePublication_publicationsExists() {
        var publication = Mockito.mock(Publication::class.java)
        var publisher = Mockito.mock(Publisher::class.java)
        publication.publisher = publisher

        publisherService.publicationRepository.addPublication(publication)
        publisherService.publicationRepository.addPublication(Mockito.mock(Publication::class.java))
        publisherService.publicationRepository.addPublication(Mockito.mock(Publication::class.java))

        assertThat(publisherService.seePublications(publisher), hasItem(publication))
    }

    @Test
    fun noPublication_createPublicationForPublisher_publicationExists() {
        var numberOfPub = publisherService.publicationRepository.getPublications().size
        var publication = Mockito.mock(Publication::class.java)
        var publisher = Mockito.mock(Publisher::class.java)

        publisherService.createPublicationForPublisher(publication, publisher)

        assertEquals(numberOfPub + 1, publisherService.publicationRepository.getPublicationByPublisher(publisher).size)
    }

    @Test
    fun publicationExists_deletePublicationForPublisher_noPublication() {
        var numberOfPub = publisherService.publicationRepository.getPublications().size
        var publication = Mockito.mock(Publication::class.java)
        var publisher = Mockito.mock(Publisher::class.java)
        publisherService.createPublicationForPublisher(publication, publisher)


        publisherService.deletePublication(publication)

        assertEquals(numberOfPub, publisherService.publicationRepository.getPublicationByPublisher(publisher).size)
    }

}